/* Coyright (c) 2004-2005  Petr Vandrovec.   Licensed under LGPL-2.1 */

#include "vmdsp_plugin.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <dirent.h>
#include <ctype.h>
#include <pthread.h>
#include <obstack.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <linux/soundcard.h>

/* Set to 1 for enabled debugging. */
#define VMDSP_DEBUG	(0)

static uid_t realUserId;

#define USE_NEXT_FN	(-2)

static FILE* fo = NULL;
static int debug = 0;

int (*vmdsp_open)(const char* filename, int flags, mode_t mode) = NULL;
int (*vmdsp_open64)(const char* filename, int flags, mode_t mode) = NULL;
off_t (*vmdsp_lseek)(int fildes, off_t offset, int whence) = NULL;
off64_t (*vmdsp_lseek64)(int fildes, off64_t offset, int whence) = NULL;
ssize_t (*vmdsp_read)(int fildes, void* buffer, size_t size) = NULL;
ssize_t (*vmdsp_write)(int fildes, const void* buffer, size_t size) = NULL;
int (*vmdsp_ioctl)(int fildes, unsigned long int command, void* param) = NULL;
int (*vmdsp_close)(int fildes) = NULL;
#ifdef NEED_FOPEN_WRAPPERS
FILE* (*vmdsp_fopen)(const char* filename, const char* mode) = NULL;
#endif
FILE* (*vmdsp_fopen64)(const char* filename, const char* mode) = NULL;
#ifdef NEED_DIROP_WRAPPERS
DIR* (*vmdsp_opendir)(const char* filename) = NULL;
struct dirent* (*vmdsp_readdir)(DIR* dirp) = NULL;
struct dirent64* (*vmdsp_readdir64)(DIR* dirp) = NULL;
struct dirent64_21* (*vmdsp_readdir64_21)(DIR* dirp) = NULL;
int (*vmdsp_closedir)(DIR* dirp) = NULL;
void (*vmdsp_rewinddir)(DIR* dirp) = NULL;
void (*vmdsp_seekdir)(DIR* dirp, long int offset) = NULL;
long int (*vmdsp_telldir)(DIR* dirp) = NULL;
#endif

static int allow_mixer_passthrough = 0;
static char* vmdsp_plugin_name;

void
vmdsp_log(const char* msg, ...) {
   if (debug) {
      if (fo) {
         va_list va;

         va_start(va, msg);
         vfprintf(fo, msg, va);
         va_end(va);
         fflush(fo);
      }
   }
}


void
vmdsp_warn(const char* msg, ...) {
   if (debug) {
      va_list va;

      va_start(va, msg);
      vfprintf(stderr, msg, va);
      if (fo) {
         vfprintf(fo, msg, va);
         fflush(fo);
      }
      va_end(va);
   }
}


static inline uid_t
getSuperUser(void)
{
   return geteuid();
}


static inline void
restoreSuperUser(uid_t euid)
{
   int savedErrno = errno;
   seteuid(euid);
   errno = savedErrno;
}


static inline uid_t
becomeSuperUser(void)
{
   uid_t su;

   su = getSuperUser();
   if (su) {
      restoreSuperUser(0);
   }
   return su;
}

static void
restoreNormalUser(uid_t euid)
{
   if (euid == 0) {
      seteuid(euid);
   }
}

static uid_t
becomeNormalUser(void)
{
   uid_t euid;

   euid = geteuid();
   if (euid == 0) {
      seteuid(realUserId);
   }
   return euid;
}

static int isVMX = 0;
static char buf[4096];

static void initVmxGlue(void) {
        FILE* f;
        size_t bl = 0;
        struct stat stb;

        f = vmdsp_fopen64("/proc/self/cmdline", "rb");
        if (!f) {
                /* Failed to open cmdline? It cannot be vmx... */
                return;
        }
        while (fread(buf + bl, 1, 1, f) == 1) {
                if (buf[bl] == 0) {
                        break;
                }
                if (buf[bl] == '/') {
                        bl = 0;
                        continue;
                }
                bl++;
                if (bl >= sizeof(buf) - 1) {
                        break;
                }
        }
        buf[bl] = 0;
        if (memcmp(buf, "vmware-vmx", 10)) {
                /* It is not vmware-vmx... */
                fclose(f);
                return;
        }
        bl = 0;
        while (fread(buf + bl, 1, 1, f) == 1) {
                if (buf[bl] == 0) {
                        bl = 0;
                        continue;
                }
                if (bl < sizeof(buf) - 1) {
                        bl++;
                }
        }
        fclose(f);
        if (bl != 0 || buf[0] != '/') {
                /* Last argument did not start with slash */
                return;
        }
        if (access(buf, R_OK)) {
                /* No access to file?! */
                return;
        }
        if (stat(buf, &stb)) {
                /* File not found */
                return;
        }
        if (!S_ISREG(stb.st_mode)) {
                /* Not regular file */
                return;
        }
        /* Fine, that's us... */
        isVMX = 1;
}

const char* vmxGetConfig(void) {
   return isVMX ? buf : NULL;
}

#ifdef NEED_DIROP_WRAPPERS
struct dirent64_21 {
   __ino_t		d_ino;
   __off64_t		d_off;
   unsigned short int	d_reclen;
   unsigned char	d_type;
   char			d_name[256];           /* We must not include limits.h! */
};

static void
dirent64_to_dirent64_21(struct dirent64_21* dst,	// OUT: glibc2.1 dirent
			const struct dirent64* src) {	// IN:  actual dirent
   size_t namelen;

   namelen = src->d_reclen - offsetof(struct dirent64, d_name);
   dst->d_ino = src->d_ino;
   dst->d_off = src->d_off;
   dst->d_reclen = namelen + offsetof(struct dirent64_21, d_name);
   dst->d_type = src->d_type;
   memcpy(dst->d_name, src->d_name, namelen);
}

static void
dirent64_to_dirent(struct dirent* dst,			// OUT: actual dirent
		   const struct dirent64* src) {	// IN:  glibc2.1 dirent
   size_t namelen;

   namelen = src->d_reclen - offsetof(struct dirent64, d_name);
   dst->d_ino = src->d_ino;
   dst->d_off = src->d_off;
   dst->d_reclen = namelen + offsetof(struct dirent, d_name);
   dst->d_type = src->d_type;
   memcpy(dst->d_name, src->d_name, namelen);
}
#endif

static u_int32_t spfd[1024];

struct fdfns {
   off64_t (*lseek)(void* cookie, off64_t offset, int whence);
   ssize_t (*read)(void* cookie, void* buffer, size_t size);
   ssize_t (*write)(void* cookie, const void* buffer, size_t size);
   int     (*ioctl)(void* cookie, unsigned long int command, void* data);
   void    (*cleanup)(void* cookie);
};

struct mystr {
	struct mystr* next;
	unsigned int fildes;
	int usecnt;
	void* cookie;
	const struct fdfns* fns;
};

static struct mystr* fds;
static pthread_mutex_t fdlock = PTHREAD_MUTEX_INITIALIZER;

static struct mystr*
newfd(int fildes,			// IN: file descriptor
      void* cookie,			// IN: cookie
      const struct fdfns* fns) {	// IN: function callbacks
   struct mystr* tmp;

   tmp = malloc(sizeof(*tmp));
   tmp->next = NULL;
   tmp->fildes = fildes;
   tmp->usecnt = 1;
   tmp->cookie = cookie;
   tmp->fns = fns;
   return tmp;
}

static struct mystr*
isspecialfd(unsigned int fildes) {	// IN: file descriptor
   struct mystr* cp;

   if (fildes < sizeof(spfd) * 8) {
      if ((spfd[fildes / 32] & (1 << (fildes & 31))) == 0) {
         return NULL;
      }
   }
   pthread_mutex_lock(&fdlock);
   for (cp = fds; cp; cp = cp->next) {
      if (cp->fildes == fildes) {
         cp->usecnt++;
         pthread_mutex_unlock(&fdlock);
         return cp;
      }
      if (cp->fildes > fildes) {
         break;
      }
   }
   pthread_mutex_unlock(&fdlock);
   return NULL;
}

static void
insertspecialfd(struct mystr* p) {	// IN: descriptor structure
   struct mystr** cp;
   struct mystr* c2;
   unsigned int fildes = p->fildes;

   pthread_mutex_lock(&fdlock);
   for (cp = &fds; (c2 = *cp) != NULL; cp = &c2->next) {
      if (c2->fildes >= fildes) {
         break;
      }
   }
   p->next = c2;
   *cp = p;
   if (fildes < sizeof(spfd) * 8) {
      spfd[fildes / 32] |= 1 << (fildes & 31);
   }
   pthread_mutex_unlock(&fdlock);
}

static struct mystr*
removespecialfd(unsigned int fildes) {	// IN: file descriptor
   struct mystr** cp;
   struct mystr* c2;

   if (fildes < sizeof(spfd) * 8) {
      if ((spfd[fildes / 32] & (1 << (fildes & 31))) == 0) {
         return NULL;
      }
   }
   pthread_mutex_lock(&fdlock);
   for (cp = &fds; (c2 = *cp) != NULL; cp = &c2->next) {
      if (c2->fildes == fildes) {
         *cp = c2->next;
         c2->next = NULL;
         spfd[fildes / 32] &= ~(1 << (fildes & 31));
         pthread_mutex_unlock(&fdlock);
         return c2;
      }
      if (c2->fildes > fildes) {
         break;
      }
   }
   pthread_mutex_unlock(&fdlock);
   return NULL;
}

static void 
releasefd(struct mystr* cp) {		// IN: descriptor structure
   if (--cp->usecnt == 0) {
      if (cp->fns->cleanup) {
         cp->fns->cleanup(cp->cookie);
      }
      free(cp);
   }
}

/* ------------------------------------------------------------------------------- */
static int
volume_to_oss(int volume) {
   if (volume < 0) {
      return 0;
   } else if (volume > 32767) {
      return 100;
   } else {
      return (volume + 163) / 327;
   }
}

static int
volume_from_oss(int volume) {
   if (volume < 0) {
      return 0;
   } else if (volume > 100) {
      return 32767;
   } else {
      return volume * 327;
   }
}

static off64_t 
dsp_lseek(void* cookie, off64_t offset, int whence) {
   vmdsp_warn("lseek(dsp, 0x%llx, %u)\n", offset, whence);
   errno = ESPIPE;
   return -1;
   (void)cookie;
}

static ssize_t
dsp_read(void* cookie, void* buffer, size_t size) {
   struct vmdsp_dsp* dsp = cookie;

   return dsp->info->d_op->read(dsp, buffer, size);
}

static ssize_t
dsp_write(void* cookie, const void* buffer, size_t size) {
   struct vmdsp_dsp* dsp = cookie;

   return dsp->info->d_op->write(dsp, buffer, size);
}

static int
dsp_ioctl(void* cookie, unsigned long int command, void* data) {
   struct vmdsp_dsp* dsp = cookie;

   switch (command) {
      case SNDCTL_DSP_RESET:	/* N-P00 */
         vmdsp_log("ioctl SNDCTL_DSP_RESET\n");
	 return 0;
      case SNDCTL_DSP_SPEED:	/* W-P02 */
         vmdsp_log("ioctl SNDCTL_DSP_SPEED(%u)\n", *(const int*)data);
	 return dsp->info->d_op->set_speed(dsp, *(const int*)data);
      case SNDCTL_DSP_STEREO:	/* W-P03 */
         vmdsp_log("ioctl SNDCTL_DSP_STEREO(%u)\n", 0 != *(const int*)data);
	 return dsp->info->d_op->set_stereo(dsp, 0 != *(const int*)data);
      case SNDCTL_DSP_SETFMT:	/* W-P05 */
         {
	    int fmt = *(int*)data;
	    int err;

            vmdsp_log("ioctl SNDCTL_DSP_SETFMT(0x%08X)\n", fmt);
	    if (fmt != AFMT_QUERY) {
               err = dsp->info->d_op->set_format(dsp, fmt);
	       if (err) {
	          return err;
	       }
	    }
	    return dsp->info->d_op->get_format(dsp, data);
	 }
      default:	 
         vmdsp_warn("ioctl(dsp, 0x%08X, %p)\n", command, data);
	 break;
   }
   return USE_NEXT_FN;
}

static void
dsp_cleanup(void* cookie) {
   struct vmdsp_dsp* dsp = cookie;

   vmdsp_log("cleanup(dsp)\n");
   dsp->info->d_op->close_dsp(dsp);
   return;
}

static struct fdfns dsp_desc = {
   .lseek   = dsp_lseek,
   .read    = dsp_read,
   .write   = dsp_write,
   .ioctl   = dsp_ioctl,
   .cleanup = dsp_cleanup,
};

static off64_t 
mixer_lseek(void* cookie, off64_t offset, int whence) {
   vmdsp_warn("lseek(mixer, 0x%llx, %u)\n", offset, whence);
   errno = ESPIPE;
   return -1;
   (void)cookie;
}

static ssize_t
mixer_read(void* cookie, void* buffer, size_t size) {
   vmdsp_warn("read(mixer, %p, %u)\n", buffer, size);
   errno = EINVAL;
   return -1;
   (void)cookie;
}

static ssize_t
mixer_write(void* cookie, const void* buffer, size_t size) {
   vmdsp_warn("write(mixer, %p, %u)\n", buffer, size);
   errno = EINVAL;
   return -1;
   (void)cookie;
}

static int
mixer_ioctl(void* cookie, unsigned long int command, void* data) {
   struct vmdsp_mixer* mixer = cookie;
   int dir  = _IOC_DIR(command);
   int size = _IOC_SIZE(command);
   int type = _IOC_TYPE(command);
   int nr   = _IOC_NR(command);
   int err;
   
   if (type == 'M' && size == sizeof(int) && nr >= 0 && nr <= SOUND_MIXER_NONE) {
      if (dir == _IOC_READ) {
	 int left, right;

	 vmdsp_log("ioctl SOUND_MIXER_READ(%u)\n", nr);
	 err = mixer->info->d_op->get_volume(mixer, nr, &left, &right);
	 if (!err) {
	    left = volume_to_oss(left);
	    right = volume_to_oss(right);
	    *(int*)data = (right << 8) | left;
	 }
	 return err;
      } else if (dir == _IOC_WRITE || dir == (_IOC_READ|_IOC_WRITE)) {
	 int left, right, val;
	 
	 vmdsp_log("ioctl SOUND_MIXER_WRITE(%u)\n", nr);
	 val = *(const int*)data;
	 right = volume_from_oss((val >> 8) & 0xFF);
	 left = volume_from_oss(val & 0xFF);
	 err = mixer->info->d_op->set_volume(mixer, nr, &left, &right);
	 if (!err && dir == (_IOC_READ|_IOC_WRITE)) {
	    left = volume_to_oss(left);
	    right = volume_to_oss(right);
	    *(int*)data = (right << 8) | left;
	 }
	 return err;
      }
   }
   
   switch (command) {
      case SOUND_MIXER_READ_RECSRC:	/* R-MFF */
         vmdsp_log("ioctl SOUND_MIXER_READ_RECSRC\n");
	 err = mixer->info->d_op->get_recsrc(mixer, data);
	 return err;
      case SOUND_MIXER_WRITE_RECSRC:	/* W-MFF */
         vmdsp_log("ioctl SOUND_MIXER_WRITE_RECSRC(%d)\n", *(const int*)data);
	 err = mixer->info->d_op->set_recsrc(mixer, data);
	 return err;
      default:	 
         vmdsp_warn("ioctl(mixer, 0x%08X, %p)\n", command, data);
	 break;
   }
   return USE_NEXT_FN;
}

static void
mixer_cleanup(void* cookie) {
   struct vmdsp_mixer* mixer = cookie;
   
   vmdsp_log("cleanup(mixer)\n");
   mixer->info->d_op->close_mixer(mixer);
   return;
}

static struct fdfns mixer_desc = {
   .lseek   = mixer_lseek,
   .read    = mixer_read,
   .write   = mixer_write,
   .ioctl   = mixer_ioctl,
   .cleanup = mixer_cleanup,
};

static void* plugin_handle = NULL;
static struct vmdsp_plugin* plugin = NULL;

static struct vmdsp_plugin*
plugin_start(const char* plugin_name, const char* cfgfile) {
   struct vmdsp_plugin* (*initfunc)(const char*);

   plugin_handle = dlopen(plugin_name,  RTLD_LAZY);
   if (!plugin_handle) {
      vmdsp_warn("Unable to load %s: %s\n", plugin_name, dlerror());
      return NULL;
   }
   initfunc = dlsym(plugin_handle, "vmdsp_plugin_init");
   if (initfunc) {
      struct vmdsp_plugin* plg;
      
      plg = initfunc(cfgfile);
      if (plg) {
         return plg;
      } else {
         vmdsp_warn("Initialization of plugin %s failed: %s\n", plugin_name, strerror(errno));
      }
   } else {
      vmdsp_warn("Cannot locate function vmdsp_plugin_init in the plugin %s\n", plugin_name);
   }
//   dlclose(plugin_handle);
   plugin_handle = 0;
   return NULL;
}
      

static int
check_open(const char* filename, int flags, mode_t mode) {
   if (filename && !strcmp(filename, "/dev/dsp")) {
      struct vmdsp_dsp* f;

      vmdsp_log("open(%s, 0x%X, 0%o) [dsp]\n", filename, flags, mode);

      if (!plugin) {
         plugin = plugin_start(vmdsp_plugin_name, vmxGetConfig());
	 if (!plugin) {
	    errno = ENODEV;
	    return -1;
	 }
      }
      f = plugin->d_op->new_dsp(plugin, filename, flags & O_ACCMODE);
      if (f) {
         struct mystr* p;
	 
	 p = newfd(f->fd, f, &dsp_desc);
	 insertspecialfd(p);
	 return f->fd;
      }
      return -1;
   }
   if (filename && !strcmp(filename, "/dev/mixer")) {
      vmdsp_log("open(%s, 0x%X, 0%o) [mixer]\n", filename, flags, mode);

      if (!plugin) {
         plugin = plugin_start(vmdsp_plugin_name, vmxGetConfig());
      }
      if (plugin && plugin->d_op->new_mixer) {
         struct vmdsp_mixer* f;

         f = plugin->d_op->new_mixer(plugin, filename, flags & O_ACCMODE);
         if (f) {
            struct mystr* p;

            p = newfd(f->fd, f, &mixer_desc);
	    insertspecialfd(p);
	    return f->fd;
         }
         return -1;
      }
      if (!allow_mixer_passthrough) {
         errno = ENODEV;
	 return -1;
      }
   }	
   return USE_NEXT_FN;
}

static off64_t check_lseek(int fildes, off64_t offset, int whence) {
	struct mystr* ms;
	off64_t ret = USE_NEXT_FN;
	
	ms = isspecialfd(fildes);
	if (ms) {
		vmdsp_log("lseek(%u, 0x%llX, %u)\n", fildes, offset, whence);
		if (ms->fns->lseek) {
			ret = ms->fns->lseek(ms->cookie, offset, whence);
		}
		releasefd(ms);
	}
	return ret;
}

static ssize_t check_read(int fildes, void* buffer, size_t size) {
	struct mystr* ms;
	ssize_t ret = USE_NEXT_FN;
	
	ms = isspecialfd(fildes);
	if (ms) {
		vmdsp_log("read(%u, %p, %u)\n", fildes, buffer, size);
		if (ms->fns->read) {
			ret = ms->fns->read(ms->cookie, buffer, size);
		}
		releasefd(ms);
	}
	return ret;
}

static ssize_t check_write(int fildes, const void* buffer, size_t size) {
	struct mystr* ms;
	ssize_t ret = USE_NEXT_FN;
	
	ms = isspecialfd(fildes);
	if (ms) {
		vmdsp_log("write(%u, %p, %u)\n", fildes, buffer, size);
		if (ms->fns->write) {
			ret = ms->fns->write(ms->cookie, buffer, size);
		}
		releasefd(ms);
	}
	return ret;
}

static int check_ioctl(int fildes, unsigned long int command, void* param) {
	struct mystr* ms;
	int ret = USE_NEXT_FN;
	
	ms = isspecialfd(fildes);
	if (ms) {
		vmdsp_log("ioctl(%u, 0x%08lX, %p)\n", fildes, command, param);
		if (ms->fns->ioctl) {
			ret = ms->fns->ioctl(ms->cookie, command, param);
		}		
		releasefd(ms);
	}
	return ret;
}

static int check_close(int fildes) {
	struct mystr* ms;
	
	ms = removespecialfd(fildes);
	if (ms) {
		vmdsp_log("close(%u)\n", fildes);
		releasefd(ms);
		return 0;
	}
	return USE_NEXT_FN;
}

#ifdef NEED_FOPEN_WRAPPERS
#if 0
static const cookie_io_functions_t deiofns = {
	.read = deio_reader,
	.write = deio_writer,
	.seek = deio_seeker,
	.close = deio_closer,
};
#endif

static int check_fopen(FILE** pf, const char* filename, const char* mode) {
#if 0
	if (filename && !strncmp(filename, pbud, pbud_len)) {
		const char* f2 = filename + pbud_len;

		vmdsp_log("fopen(%s, %s)\n", filename, mode);

		if (!strcmp(f2, "devices")) {
			FILE* q;
			
			q = vmdsp_fopen(filename, mode);
			if (q) {
				struct devinfo* di;
				
				di = malloc(sizeof(*di));
				deio_init(di, q);
				*pf = fopencookie(di, mode, deiofns);
				vmdsp_log("... pf=%p, cookie=%p\n", *pf, di);
				if (!*pf) {
					deio_closer(di);
				}
				return 0;
			}
			*pf = NULL;
			return 0;
		} else {
			const char* rest;
			int busid, devid;
			
			rest = getbid(f2, &busid, &devid);
			if (!*rest && usbdev_exist(busid, devid)) {
				///
			}
		}
	}
#endif
	return USE_NEXT_FN;
}
#endif

int open(const char* filename, int flags, ...) {
   mode_t mode = 0;
   int r;

   if (flags & O_CREAT) {
      va_list va;

      va_start(va, flags);
      mode = va_arg(va, mode_t);
      va_end(va);
   }

   vmdsp_log("open(\"%s\", 0x%X, 0%o)\n", filename, flags, mode);

   if (isVMX) {
      r = check_open(filename, flags, mode);
      if (r != USE_NEXT_FN) {
         return r;
      }
   }
   return vmdsp_open(filename, flags, mode);
}

int open64_21(const char* filename, int flags, ...) {
   mode_t mode = 0;
   int r;

   if (flags & O_CREAT) {
      va_list va;

      va_start(va, flags);
      mode = va_arg(va, mode_t);
      va_end(va);
   }

   vmdsp_log("open64(\"%s\", 0x%X, 0%o)\n", filename, flags, mode);

   if (isVMX) {
      r = check_open(filename, flags | O_LARGEFILE, mode);
      if (r != USE_NEXT_FN) {
         return r;
      }
   }
   return vmdsp_open64(filename, flags, mode);
}
#ifdef __x86_64__
asm(".symver open64_21, open64@GLIBC_2.2.5");
#else
/* glibc exports open64@GLIBC_2.1 while pthreads export open64@GLIBC_2.2... */
asm(".symver open64_21, open64@GLIBC_2.1");
asm(".set open64_22, open64_21; .global open64_22");
asm(".symver open64_22, open64@GLIBC_2.2");
#endif

off_t lseek(int fildes, off_t offset, int whence) {
   off64_t r;

   r = check_lseek(fildes, offset, whence);
   if (r == USE_NEXT_FN) {
      return vmdsp_lseek(fildes, offset, whence);
   }
   return r;
}

off64_t lseek64_21(int fildes, off64_t offset, int whence) {
   off64_t r;

   r = check_lseek(fildes, offset, whence);
   if (r == USE_NEXT_FN) {
      return vmdsp_lseek64(fildes, offset, whence);
   }
   return r;
}
#ifdef __x86_64__
asm(".symver lseek64_21, lseek64@GLIBC_2.2.5");
#else
/* glibc exports lseek64@GLIBC_2.1 while pthreads export lseek64@GLIBC_2.2... */
asm(".symver lseek64_21, lseek64@GLIBC_2.1");
asm(".set lseek64_22, lseek64_21; .global lseek64_22");
asm(".symver lseek64_22, lseek64@GLIBC_2.2");
#endif

ssize_t read(int fildes, void* buffer, size_t size) {
   ssize_t r;

   r = check_read(fildes, buffer, size);
   if (r == USE_NEXT_FN) {
      return vmdsp_read(fildes, buffer, size);
   }
   return r;
}

ssize_t write(int fildes, const void* buffer, size_t size) {
   ssize_t r;

   r = check_write(fildes, buffer, size);
   if (r == USE_NEXT_FN) {
      return vmdsp_write(fildes, buffer, size);
   }
   return r;
}

int ioctl(int fildes, unsigned long int command, ...) {
   void* arg;
   va_list va;
   int r;

   va_start(va, command);
   arg = va_arg(va, void*);
   va_end(va);
   r = check_ioctl(fildes, command, arg);
   if (r == USE_NEXT_FN) {
      return vmdsp_ioctl(fildes, command, arg);
   }
   return r;
}

int close(int fildes) {
   int r;

   r = check_close(fildes);
   if (r == USE_NEXT_FN) {
      return vmdsp_close(fildes);
   }
   return r;
}

#ifdef NEED_FOPEN_WRAPPERS

FILE* fopen(const char* path, const char* mode) {
   FILE* f;

   vmdsp_log("fopen(\"%s\", \"%s\")\n", path, mode);

   if (isVMX && check_fopen(&f, path, mode) != USE_NEXT_FN) {
      return f;
   }
   return vmdsp_fopen(path, mode);
}

FILE* fopen64(const char* path, const char* mode) {
   FILE* f;

   vmdsp_log("fopen64(\"%s\", \"%s\")\n", path, mode);

   if (isVMX && check_fopen(&f, path, mode) != USE_NEXT_FN) {
      return f;
   }
   return vmdsp_fopen64(path, mode);
}
#endif

#ifdef NEED_DIROP_WRAPPERS

static pthread_mutex_t dirop_lock = PTHREAD_MUTEX_INITIALIZER;

struct dirop_fn {
   int (*readdir64)(void* cookie, struct dirent64** de);
   void (*cleanup)(void* cookie);
};

#if 0
struct dirop_usb {
   DIR* dirp;
   int dofake;
};

static int dirop_usb_readdir64(void* cookie, struct dirent64** de) {
   struct dirop_usb* du = cookie;
   struct dirent64* d;

   if (du->dirp) {
      d = vmdsp_readdir64(du->dirp);
   } else {
		d = NULL;
	}
	if (!d && du->dofake >= 0) {
		static struct dirent64 dp;
		int bnum;
		
		bnum = usbbus_next(du->dofake);
		if (bnum < 0) {
			du->dofake = -1;
		} else {
			du->dofake = bnum + 1;
		}

		if (bnum >= 0) {
			dp.d_ino = 0xDEADBEAF;
			dp.d_off = 0xDEADBEAF;
			sprintf(dp.d_name, "%03u", bnum);
			dp.d_reclen = strlen(dp.d_name) + offsetof(struct dirent64, d_name) + 1;
			dp.d_type = DT_DIR;		
			d = &dp;
		} else {
			d = NULL;
		}
	}
	*de = d;
	return 0;
}

static void dirop_usb_cleanup(void* cookie) {
	struct dirop_usb* du = cookie;
	
	free(du);
}

static struct dirop_fn dirop_usb_ops = {
	.readdir64 = dirop_usb_readdir64,
	.cleanup = dirop_usb_cleanup,
};

struct dirop_devd {
	int pos;
	int busid;
	struct dirent64 de;
};

static int dirop_devd_readdir64(void* cookie, struct dirent64** de) {
	struct dirop_devd* dd = cookie;
	
	switch (dd->pos) {
		case 0:
			strcpy(dd->de.d_name, ".");
			dd->de.d_type = DT_DIR;
			break;
		case 1:
			strcpy(dd->de.d_name, "..");
			dd->de.d_type = DT_DIR;
			break;
		default:
			{
				int devid = usbdev_getidx(dd->busid, dd->pos - 2);
				
				if (devid < 0) {
					*de = NULL;
					return 0;
				}
				sprintf(dd->de.d_name, "%03u", devid);
			}
			dd->de.d_type = DT_REG;
			break;
	}
	dd->pos++;
	dd->de.d_ino = 0xDEADBEAF;
	dd->de.d_off = dd->pos;
	dd->de.d_reclen = strlen(dd->de.d_name) + offsetof(struct dirent64, d_name) + 1;
	*de = &dd->de;
	return 0;
}	
	
static void dirop_devd_cleanup(void* cookie) {
	struct dirop_devd* dd = cookie;
	
	free(dd);
}

static struct dirop_fn dirop_devd_ops = {
	.readdir64 = dirop_devd_readdir64,
	.cleanup = dirop_devd_cleanup,
};
#endif

struct dirop_info {
	struct dirop_info* next;
	DIR* dirp;
	int usecnt;
	
	void* cookie;
	const struct dirop_fn* dfn;
};

static struct dirop_info* dirop_list;

static void dirop_insert(DIR* dirp, void* cookie, const struct dirop_fn* dfn) {
	struct dirop_info* di;
	
	di = malloc(sizeof(*di));
	di->next = NULL;
	di->dirp = dirp;
	di->usecnt = 1;
	di->cookie = cookie;
	di->dfn = dfn;
	
	pthread_mutex_lock(&dirop_lock);
	di->next = dirop_list;
	dirop_list = di;
	pthread_mutex_unlock(&dirop_lock);
}

static struct dirop_info* dirop_isspecial(DIR* dirp) {
	struct dirop_info* cp;

	pthread_mutex_lock(&dirop_lock);
	for (cp = dirop_list; cp; cp = cp->next) {
		if (cp->dirp == dirp) {
			cp->usecnt++;
			pthread_mutex_unlock(&dirop_lock);
			return cp;
		}
	}
	pthread_mutex_unlock(&dirop_lock);
	return NULL;
}

static struct dirop_info* dirop_closing(DIR* dirp) {
	struct dirop_info** c2;
	struct dirop_info* cp;
	
	pthread_mutex_lock(&dirop_lock);
	for (c2 = &dirop_list; (cp = *c2) != NULL; c2 = &cp->next) {
		if (cp->dirp == dirp) {
			*c2 = cp->next;
			cp->next = NULL;
			pthread_mutex_unlock(&dirop_lock);
			return cp;
		}
	}
	pthread_mutex_unlock(&dirop_lock);
	return NULL;
}

static void dirop_release(struct dirop_info* di) {
	if (--di->usecnt == 0) {
		if (di->dfn->cleanup)
			di->dfn->cleanup(di->cookie);
		free(di);
	}
}
	
DIR* opendir(const char* filename) {
	vmdsp_log("opendir(\"%s\")\n", filename);

#if 0
	if (filename && !strncmp(filename, pbud, pbud_len - 1)) {
		const char* f2 = filename + pbud_len - 1;

		vmdsp_log("opendir(%s)\n", filename);

		if (!strcmp(f2, "") || !strcmp(f2, "/")) {
			DIR* dirp;
			
			dirp = vmdsp_opendir(filename);
			if (dirp) {
				struct dirop_usb* du;
				
				du = malloc(sizeof(*du));
				du->dirp = dirp;
				du->dofake = 0;
				dirop_insert(dirp, du, &dirop_usb_ops);
			} else if (errno == ENOENT) {
				struct dirop_usb* du;

				dirp = vmdsp_opendir("/");
				if (!dirp) {
					return dirp;
				}
				du = malloc(sizeof(*du));
				du->dirp = NULL;
				du->dofake = 0;
				dirop_insert(dirp, du, &dirop_usb_ops);
			}
			return dirp;
		} else if (f2[0] == '/') {
			const char* rest;
			int busid, devid;
			
			rest = getbid(f2 + 1, &busid, &devid);
			if (*rest == 0 && usbbus_exist(busid) && devid == -1) {
				DIR* dirp;

				dirp = vmdsp_opendir(".");
				if (dirp) {
					struct dirop_devd* du;

					du = malloc(sizeof(*du));
					du->pos = 0;
					du->busid = busid;
					dirop_insert(dirp, du, &dirop_devd_ops);
				}
				return dirp;
			}
		} else {
		}
	}
#endif
	return vmdsp_opendir(filename);
}

struct dirent* readdir(DIR* dirp) {
	struct dirop_info* di;
	
	di = dirop_isspecial(dirp);
	if (di) {
		int r = USE_NEXT_FN;
		struct dirent64* de64 = NULL;
		
		if (di->dfn->readdir64) {
			r = di->dfn->readdir64(di->cookie, &de64);
		}
		dirop_release(di);
		if (r != USE_NEXT_FN) {
			if (de64) {
				static struct dirent de;

				dirent64_to_dirent(&de, de64);
				return &de;
			}
			return NULL;
		}
	}
	return vmdsp_readdir(dirp);	
}

struct dirent64* readdir64_22(DIR* dirp) {
	struct dirop_info* di;
	
	di = dirop_isspecial(dirp);
	if (di) {
		int r = USE_NEXT_FN;
		struct dirent64* de = NULL;
		
		if (di->dfn->readdir64) {
			r = di->dfn->readdir64(di->cookie, &de);
		}
		dirop_release(di);
		if (r != USE_NEXT_FN) {
			return de;
		}
	}
	return vmdsp_readdir64(dirp);
}
asm(".symver readdir64_22, readdir64@GLIBC_2.2");

struct dirent64_21* readdir64_21(DIR* dirp) {
	struct dirop_info* di;
	
	di = dirop_isspecial(dirp);
	if (di) {
		int r = USE_NEXT_FN;
		struct dirent64* de = NULL;
		
		if (di->dfn->readdir64) {
			r = di->dfn->readdir64(di->cookie, &de);
		}
		dirop_release(di);
		if (r != USE_NEXT_FN) {
			if (de) {
				static struct dirent64_21 de21;
				
				dirent64_to_dirent64_21(&de21, de);
				return &de21;
			}
			return NULL;
		}
	}
	return vmdsp_readdir64_21(dirp);
}

asm(".symver readdir64_21, readdir64@GLIBC_2.1");

void rewinddir(DIR* dirp) {
	struct dirop_info* di;
	
	di = dirop_isspecial(dirp);
	if (di) {
		///
		dirop_release(di);
	}
	vmdsp_rewinddir(dirp);
}

void seekdir(DIR* dirp, long int offset) {
	struct dirop_info* di;
	
	di = dirop_isspecial(dirp);
	if (di) {
		///
		dirop_release(di);
	}
	vmdsp_seekdir(dirp, offset);
}

long int telldir(DIR* dirp) {
	struct dirop_info* di;
	
	di = dirop_isspecial(dirp);
	if (di) {
		///
		dirop_release(di);
	}
	return vmdsp_telldir(dirp);
}

int closedir(DIR* dirp) {
	struct dirop_info* di;
	
	di = dirop_closing(dirp);
	if (di) {
		///
		dirop_release(di);
	}
	return vmdsp_closedir(dirp);
}
#endif

static void findsym(void* dstp, const void* disallowed, const char* sym) {
	void* ptr;
	void** dst = dstp;
	
	*dst = NULL;
	ptr = dlsym(RTLD_NEXT, sym);
	if (ptr && ptr != disallowed) {
		*dst = ptr;
	}
	if (*dst == NULL) {
		fprintf(stderr, "Unable to get %s...\n", sym);
		exit(0);
	}
}

#ifdef NEED_DIROP_WRAPPERS
static void findvsym(void* dstp, const void* disallowed, const char* sym, const char* version) {
	void* ptr;
	void** dst = dstp;
	
	*dst = NULL;
	ptr = dlvsym(RTLD_NEXT, sym, version);
	if (ptr && ptr != disallowed) {
		*dst = ptr;
	}
	if (*dst == NULL) {
		fprintf(stderr, "Unable to get %s@%s...\n", sym, version);
		exit(0);
	}
}
#endif

static void __attribute__((constructor)) init(void) {
	const char* dbg;
	uid_t uid;
	const char* bke;
	const char* p;

	realUserId = getuid();

	uid = becomeNormalUser();
	if (VMDSP_DEBUG) {
		dbg = getenv("VMDSP_DEBUG");
	} else {
		dbg = NULL;
	}
	if (dbg && !strcmp(dbg, "yes")) {
		debug = 1;
	} else {
		debug = 0;
	}

	findsym(&vmdsp_open,		open,		"open");
	findsym(&vmdsp_open64,		open64_21,	"open64");
	findsym(&vmdsp_lseek,		lseek,		"lseek");
	findsym(&vmdsp_lseek64,		lseek64_21,	"lseek64");
	findsym(&vmdsp_read,		read,		"read");
	findsym(&vmdsp_write,		write,		"write");
	findsym(&vmdsp_close,		close,		"close");
	findsym(&vmdsp_ioctl,		ioctl,		"ioctl");
#ifdef NEED_FOPEN_WRAPPERS
	findsym(&vmdsp_fopen,		fopen,		"fopen");
	findsym(&vmdsp_fopen64,		fopen64,	"fopen64");
#else
	findsym(&vmdsp_fopen64,		NULL,           "fopen64");
#endif
#ifdef NEED_DIROP_WRAPPERS
	findsym(&vmdsp_opendir,		opendir,	"opendir");
	findsym(&vmdsp_readdir,		readdir,	"readdir");
	findvsym(&vmdsp_readdir64_21,	readdir64_21,	"readdir64", "GLIBC_2.1");
	findvsym(&vmdsp_readdir64,	readdir64_22,	"readdir64", "GLIBC_2.2");
	findsym(&vmdsp_telldir,		telldir,	"telldir");
	findsym(&vmdsp_seekdir,		seekdir,	"seekdir");
	findsym(&vmdsp_rewinddir,	rewinddir,	"rewinddir");
	findsym(&vmdsp_closedir,	closedir,	"closedir");
#endif

	initVmxGlue();

	if (debug) {
		fo = vmdsp_fopen64("/tmp/vmdsp.log", "at");
	}
	p = getenv("VMDSP_MIXER_PASSTHROUGH");
	if (p && !strcmp(p, "yes")) {
	   allow_mixer_passthrough = 1;
	}
	bke = getenv("VMDSP_BACKEND");
	if (!bke) {
	   bke = "none";
	}
	for (p = bke; *p; p++) {
	   if (*p >= '0' && *p <= '9') {
	      continue;
	   }
	   if (*p >= 'a' && *p <= 'z') {
	      continue;
	   }
	   if (*p >= 'A' && *p <= 'Z') {
	      continue;
	   }
	   bke = "none";
	   break;
        }
	asprintf(&vmdsp_plugin_name, "libvmdsp_%s.so", bke);
#if 0
	pthread_atfork(NULL, NULL, childCleanup);
#endif
	{
		uid_t ruid, euid, suid;

		getresuid(&ruid, &euid, &suid);
		vmdsp_log("ruid=%u, euid=%u, suid=%u [buid=%u] [vmx=%s]\n", ruid, euid, suid, uid, isVMX ? "yes" : "no");
	}
#if 0
	usbcontrolInit();
	readFile(vmxGetConfig());
#endif	
	restoreNormalUser(uid);
}

