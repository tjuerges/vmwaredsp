/* Coyright (c) 2004-2005  Petr Vandrovec.   Licensed under LGPL-2.1 */

#ifndef __VMDSP_PLUGIN_H__
#define __VMDSP_PLUGIN_H__

#define _GNU_SOURCE
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <sys/types.h>

struct vmdsp_plugin;

struct vmdsp_dsp {
   struct vmdsp_plugin* info;
   int fd;
};

struct vmdsp_mixer {
   struct vmdsp_plugin* info;
   int fd;
};

struct vmdsp_operations {
   int (*cleanup)(struct vmdsp_plugin* info);

   struct vmdsp_dsp* (*new_dsp)(struct vmdsp_plugin* info, const char* name, int mode);
   struct vmdsp_mixer* (*new_mixer)(struct vmdsp_plugin* info, const char* name, int mode);

   int (*close_dsp)(struct vmdsp_dsp* dsp);
   int (*close_mixer)(struct vmdsp_mixer* mixer);

   ssize_t (*read)(struct vmdsp_dsp* dsp, void* buffer, size_t length);
   ssize_t (*write)(struct vmdsp_dsp* dsp, const void* buffer, size_t length);

   int (*set_speed)(struct vmdsp_dsp* dsp, int speed);
   int (*set_stereo)(struct vmdsp_dsp* dsp, int stereo);
   int (*set_format)(struct vmdsp_dsp* dsp, int format);
   
   int (*get_format)(struct vmdsp_dsp* dsp, int* format);

   int (*set_volume)(struct vmdsp_mixer* mixer, int channel, int* left, int* right);
   int (*get_volume)(struct vmdsp_mixer* mixer, int channel, int* left, int* right);

   int (*set_recsrc)(struct vmdsp_mixer* mixer, int* channel);
   int (*get_recsrc)(struct vmdsp_mixer* mixer, int* channel);
};

struct vmdsp_plugin {
   const struct vmdsp_operations* d_op;
   int usecnt;
};


#ifdef __cplusplus
extern "C" {
#endif

void
vmdsp_log(const char* msg, ...);

void
vmdsp_warn(const char* msg, ...);


struct vmdsp_plugin* vmdsp_plugin_init(const char *cfgfile);

extern int (*vmdsp_open)(const char* filename, int flags, mode_t mode);
extern int (*vmdsp_open64)(const char* filename, int flags, mode_t mode);
extern off_t (*vmdsp_lseek)(int fildes, off_t offset, int whence);
extern off64_t (*vmdsp_lseek64)(int fildes, off64_t offset, int whence);
extern ssize_t (*vmdsp_read)(int fildes, void* buffer, size_t size);
extern ssize_t (*vmdsp_write)(int fildes, const void* buffer, size_t size);
extern int (*vmdsp_ioctl)(int fildes, unsigned long int command, void* param);
extern int (*vmdsp_close)(int fildes);

#ifdef __cplusplus
};
#endif


#endif	/* __VMDSP_PLUGIN_H__ */
