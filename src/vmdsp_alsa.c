/*
 * Copyright (2007) Thomas Juerges, thomas@senmut.net
 * Licensed under LGPL 2.1.
 *
 * $Id$
 */

#include "vmdsp_plugin.h"

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <linux/soundcard.h>
#include <alsa/asoundlib.h>

static const char* playback_interface = "dmix\0";
static const char* record_interface = "dsnoop\0";

struct alsap_plugin
{
	struct vmdsp_plugin g;
	struct
	{
		int left, right;
	} volume, igain, mic;
};

struct alsap_dsp
{
	struct vmdsp_dsp g;
	int speed;
	int stereo;
	int format;
	int mode;
	int playback_init;
	int record_init;
	snd_pcm_t* playback_handle;
	snd_pcm_t* record_handle;
	snd_pcm_format_t playback_format;
	snd_pcm_format_t record_format;
};

struct alsap_mixer
{
	struct vmdsp_mixer g;
};

#define container_of(ptr, type, member) \
({ \
	const typeof(((type *)0)->member)*__mptr = (ptr); \
	(type*)((char*)__mptr - offsetof(type,member)); \
})

#define INFO2ALSA(x) container_of(x, struct alsap_plugin, g)
#define ALSAP(x) struct alsap_plugin* alsap = INFO2ALSA(x)

#define DSP2ALSA(x) container_of(x, struct alsap_dsp, g)
#define ALSADSPP struct alsap_dsp* dspp = DSP2ALSA(dsp)

#define MIXER2ALSA(x) container_of(x, struct alsap_mixer, g)
#define ALSAMIXERP struct alsap_mixer* mixerp = MIXER2ALSA(mixer)

static int alsap_cleanup(struct vmdsp_plugin* info)
{
	ALSAP(info);

	free(alsap);

	return 0;
}

static struct vmdsp_dsp* alsap_new_dsp(struct vmdsp_plugin* info,
	const char* name, int mode)
{
	ALSAP(info);
	struct alsap_dsp* dspp;

	dspp = malloc(sizeof(*dspp));
	if(dspp != NULL)
	{
		int fd;

		memset(dspp, 0, sizeof(*dspp));
		dspp->g.info = &alsap->g;

		fd = vmdsp_open64("/dev/null", mode, 0);
		if(fd < 0)
		{
			free(dspp);
			errno = ENODEV;

			return NULL;
		}

		dspp->g.fd = fd;
		dspp->speed = 22050;
		dspp->format = AFMT_U8;
		dspp->mode = mode;

		return &dspp->g;
	}

	return NULL;
	(void)name;
}

static int alsap_close_dsp(struct vmdsp_dsp* dsp)
{
	ALSADSPP;

	if(dspp->playback_init != 0)
	{
		snd_pcm_close(dspp->playback_handle);
		dspp->playback_init = 0;
	}

	if(dspp->record_init != 0)
	{
		snd_pcm_close(dspp->record_handle);
		dspp->record_init = 0;
	}

	vmdsp_close(dspp->g.fd);
	free(dspp);

	return 0;
}

static int setup_hardware(struct alsap_dsp* dspp, snd_pcm_t* handle)
{
	snd_pcm_hw_params_t *hw_params;
	snd_pcm_format_t format;
	snd_pcm_access_t interleaved;
	unsigned int channels;
	unsigned int rate;
	int err;

	if(dspp->format == AFMT_U8)
	{
	   format = SND_PCM_FORMAT_U8;
	}
	else
	{
	   format = SND_PCM_FORMAT_S16_LE;
	}

	rate = (unsigned int)(dspp->speed);

	if(dspp->stereo > 0)
	{
		interleaved = SND_PCM_ACCESS_RW_INTERLEAVED;
		channels = 2;
	}
	else
	{
		interleaved = SND_PCM_ACCESS_RW_NONINTERLEAVED;
		channels = 1;
	}

	if((err = snd_pcm_hw_params_malloc(&hw_params)) < 0)
	{
		vmdsp_log("cannot allocate hardware parameter structure (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((err = snd_pcm_hw_params_any(handle, hw_params)) < 0)
	{
		vmdsp_log("cannot initialize hardware parameter structure (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((
		err = snd_pcm_hw_params_set_rate_resample(handle, hw_params, 1)) < 0)
	{
		vmdsp_log("cannot set resampling (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((
		err = snd_pcm_hw_params_set_access(handle, hw_params, interleaved))	< 0)
	{
		vmdsp_log("cannot set access type (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((
		err = snd_pcm_hw_params_set_format(handle, hw_params, format))
	< 0)
	{
		vmdsp_log("cannot set sample format (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((
		err = snd_pcm_hw_params_set_rate_near(handle, hw_params, &rate, 0))
	< 0)
	{
		vmdsp_log("cannot set sample rate (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((err = snd_pcm_hw_params_set_channels(handle, hw_params, channels)) < 0)
	{
		vmdsp_log("cannot set channel count (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	if((err = snd_pcm_hw_params(handle, hw_params)) < 0)
	{
		vmdsp_log("cannot set parameters (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	snd_pcm_hw_params_free(hw_params);

	if((err = snd_pcm_prepare(handle)) < 0)
	{
		vmdsp_log("Cannot prepare audio interface for use (%s)\n",
			 snd_strerror(err));

		return -1;
	}

	return 0;
}

static int dspp_start_playback(struct alsap_dsp* dspp)
{
	int err;

	if((dspp->mode != O_RDWR) && (dspp->mode != O_WRONLY))
	{
		vmdsp_log("Would not start playback: bad mode\n");
		errno = EINVAL;

		return -1;
	}

	if((err = snd_pcm_open(
		&(dspp->playback_handle), playback_interface, SND_PCM_STREAM_PLAYBACK, 0)) < 0)
	{
		vmdsp_log("Would not start playback: stream open failed (%s)\n",
			snd_strerror(err));
		errno = -EINVAL;

		return -1;
	}

	err = setup_hardware(dspp, dspp->playback_handle);
	if(err != 0)
	{
		errno = -EINVAL;

		return -1;
	}

	dspp->playback_init = 1;
	vmdsp_log("Playback stream %p opened\n", dspp->playback_handle);

	return 0;
}

static int dspp_start_record(struct alsap_dsp* dspp)
{
	int err;

   if((dspp->mode != O_RDWR) && (dspp->mode != O_RDONLY))
   {
	   vmdsp_log("Would not start recording: bad mode\n");
	   errno = EINVAL;

	   return -1;
   	}

   if((
   		err = snd_pcm_open(
   			&(dspp->record_handle), record_interface, SND_PCM_STREAM_CAPTURE, 0))
	< 0)
	{
		vmdsp_log("Would not start recording: stream open failed (%s)\n",
			snd_strerror(err));
		errno = EINVAL;

		return -1;
	}

	err = setup_hardware(dspp, dspp->record_handle);
	if(err != 0)
	{
		errno = -EINVAL;

		return -1;
	}

	if((err = snd_pcm_prepare(dspp->record_handle)) < 0)
	{
		vmdsp_log("Cannot prepare audio interface for use (%s)\n",
			 snd_strerror(err));
		errno = -EINVAL;

		return -1;
	}

	dspp->record_init = 1;
	vmdsp_log("Recording stream %p opened\n", dspp->record_handle);

	return 0;
}

static ssize_t dspp_read(struct vmdsp_dsp* dsp, void* buffer, size_t length)
{
	ALSADSPP;
	snd_pcm_sframes_t frames;

	if(dspp->record_init == 0)
	{
		if(dspp_start_record(dspp) != 0)
		{
			return -1;
		}
	}

	if(dspp->stereo > 0)
	{
		frames = snd_pcm_readi(dspp->record_handle, buffer,
			snd_pcm_bytes_to_frames(dspp->record_handle, length));
	}
	else
	{
		frames = snd_pcm_readn(dspp->record_handle, buffer,
			snd_pcm_bytes_to_frames(dspp->record_handle, length));
	}

	return snd_pcm_frames_to_bytes(dspp->record_handle, frames);
}

static ssize_t dspp_write(
	struct vmdsp_dsp* dsp,
	const void* buffer,
	size_t length)
{
	ALSADSPP;
	snd_pcm_sframes_t frames;

	if(dspp->playback_init == 0)
	{
		if(dspp_start_playback(dspp) != 0)
		{
			return length;
		}
	}

	frames = snd_pcm_bytes_to_frames(dspp->playback_handle, length);
	if(frames < 0)
	{
		vmdsp_log("Frame size calculation failed (%lu bytes, %d frames, %s)\n",
					length, frames, snd_strerror(frames));
	}

	if(dspp->stereo > 0)
	{
		frames = snd_pcm_writei(dspp->playback_handle, buffer, frames);
		vmdsp_log("Playback interleaved (%lu bytes, %d frames)\n",
			length, frames);
	}
	else
	{
		frames = snd_pcm_writen(
			dspp->playback_handle, (void**)(&buffer), frames);
		vmdsp_log("Playback non-interleaved (%ul bytes, %d frames)\n",
			length, frames);
	}

	if(frames < 0)
	{
		vmdsp_warn("Did not complete playback (%s)\n", snd_strerror(frames));
		frames = snd_pcm_recover(dspp->playback_handle, frames, 0);
		vmdsp_warn("Stream recovery (%s)\n", snd_strerror(frames));

		return length;
	}

	return snd_pcm_frames_to_bytes(dspp->playback_handle, frames); 
}

static int dspp_set_speed(struct vmdsp_dsp* dsp, int speed)
{
	ALSADSPP;

	if(dspp->speed != speed)
	{
	   if((dspp->playback_init != 0) || (dspp->record_init != 0))
	   {
		   vmdsp_warn("Changing sample speed while running (%d => %d)\n",
			   dspp->speed, speed);
	   }

	   dspp->speed = speed;
	}

	return 0;
}

static int dspp_set_stereo(struct vmdsp_dsp* dsp, int stereo)
{
	ALSADSPP;

	if(dspp->stereo != stereo)
	{
		if((dspp->playback_init != 0) || (dspp->record_init != 0))
		{
			vmdsp_warn("Changing stereo while running (%u => %u)\n",
				dspp->stereo, stereo);
		}
	}

	dspp->stereo = stereo;

	return 0;
}

static int dspp_set_format(struct vmdsp_dsp* dsp, int format)
{
	ALSADSPP;

	if(dspp->format != format)
	{
		if((dspp->playback_init != 0) || (dspp->record_init != 0))
		{
			vmdsp_warn("Changing format while running (%08X => %08X)\n",
				dspp->format, format);
		}
	}

	switch(format)
	{
		case AFMT_U8:
		case AFMT_S16_LE:
		{
			dspp->format = format;
		}
		break;

		default:
		{
			dspp->format = AFMT_U8;
		}
		break;
	}

	return 0;
}

static int dspp_get_format(struct vmdsp_dsp* dsp, int* format)
{
	ALSADSPP;

	*format = dspp->format;

	return 0;
}


static const struct vmdsp_operations alsa_ops =
{
	.cleanup = alsap_cleanup,
	.new_dsp = alsap_new_dsp,
	.new_mixer = NULL,
	.close_dsp = alsap_close_dsp,
	.close_mixer = NULL,
	.read = dspp_read,
	.write = dspp_write,
	.set_speed = dspp_set_speed,
	.set_stereo = dspp_set_stereo,
	.set_format = dspp_set_format,
	.get_format = dspp_get_format,
	.set_volume = NULL,
	.get_volume = NULL,
	.set_recsrc = NULL,
	.get_recsrc = NULL,
};

struct vmdsp_plugin* vmdsp_plugin_init(
	const char* cfgName __attribute__ ((unused)))
{
	struct alsap_plugin* alsap = NULL;

	alsap = malloc(sizeof(*alsap));
	if(alsap != NULL)
	{
		memset(alsap, 0, sizeof(*alsap));
		alsap->g.d_op = &alsa_ops;

		return &alsap->g;
	}

	return NULL;
}
