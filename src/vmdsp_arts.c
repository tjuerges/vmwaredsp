/* Coyright (c) 2004-2005  Petr Vandrovec.   Licensed under LGPL-2.1 */

#include "vmdsp_plugin.h"

#include <artsc/artsc.h>

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#if 0
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <sys/socket.h>
#endif

#include <linux/soundcard.h>

struct artsp_plugin {
   struct vmdsp_plugin g;
   const char* server;
   const char* app;
};

struct artsp_dsp {
   struct vmdsp_dsp g;
   int speed;
   int stereo;
   int format;
   int mode;
   arts_stream_t rd;
   arts_stream_t wr;
};

#define container_of(ptr, type, member) ({                      \
        const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)( (char *)__mptr - offsetof(type,member) );})

#define INFO2ESD(x) container_of(x, struct artsp_plugin, g)
#define ESDP(x) struct artsp_plugin* artsp = INFO2ESD(x)

#define DSP2ESD(x) container_of(x, struct artsp_dsp, g)
#define ESDDSPP struct artsp_dsp* dspp = DSP2ESD(dsp)

static int
artsp_cleanup(struct vmdsp_plugin* info) {
   ESDP(info);

   free(artsp);
   return 0;
}

static struct vmdsp_dsp*
artsp_new_dsp(struct vmdsp_plugin* info, const char* name, int mode) {
   ESDP(info);
   struct artsp_dsp* dspp;
   
   dspp = malloc(sizeof(*dspp));
   if (dspp) {
      int fd;

      memset(dspp, 0, sizeof(*dspp));
      dspp->g.info = &artsp->g;

      fd = vmdsp_open64("/dev/null", mode, 0);
      if (fd < 0) {
         free(dspp);
         errno = ENODEV;
	 return NULL;
      }
      dspp->g.fd = fd;

      dspp->speed = 22050;
      dspp->stereo = 0;
      dspp->format = AFMT_U8;

      dspp->mode = mode;
      return &dspp->g;
   }
   return NULL;
   (void)name;
}

static int
artsp_close_dsp(struct vmdsp_dsp* dsp) {
   ESDDSPP;

   if (dspp->rd) {
      arts_close_stream(dspp->rd);
   }
   if (dspp->wr) {
      arts_close_stream(dspp->wr);
   }
   vmdsp_close(dspp->g.fd);
   free(dspp);
   return 0;
}

static int
dspp_start_playback(struct artsp_dsp* dspp) {
   ESDP(dspp->g.info);

   if (dspp->mode != O_RDWR && dspp->mode != O_WRONLY) {
      vmdsp_log("Would not start playback: bad mode\n");
      errno = EINVAL;
      return -1;
   }
   dspp->wr = arts_play_stream(dspp->speed, (dspp->format == AFMT_S16_LE) ? 16 : 8, dspp->stereo ? 2 : 1, artsp->app);
   if (!dspp->wr) {
      vmdsp_log("Would not start playback: stream open failed\n");
      errno = EINVAL;
      return -1;
   }
   vmdsp_log("Playback stream %p opened\n", dspp->wr);
   return 0;
}

static int
dspp_start_record(struct artsp_dsp* dspp) {
#if 0
   /* Disable recording: vmware-vmx does not cope correctly with blocking read */
   ESDP(dspp->g.info);

   if (dspp->mode != O_RDWR && dspp->mode != O_RDONLY) {
      vmdsp_log("Would not start recording: bad mode\n");
      errno = EINVAL;
      return -1;
   }
   dspp->rd = arts_record_stream(dspp->speed, (dspp->format == AFMT_S16_LE) ? 16 : 8, dspp->stereo ? 2 : 1, artsp->app);
   if (!dspp->rd) {
      vmdsp_log("Would not start recording: stream open failed\n");
      errno = EINVAL;
      return -1;
   }
   vmdsp_log("Recording stream %p opened\n", dspp->rd);
   return 0;
#else
   errno = EINVAL;
   return -1;
   (void)dspp;
#endif
}

static ssize_t
dspp_read(struct vmdsp_dsp* dsp, void* buffer, size_t length) {
   ESDDSPP;

   if (!dspp->rd) {
      if (dspp_start_record(dspp)) {
	 return -1;
      }
   }
   if (length > 512) {
      length = 512;
   }
   return arts_read(dspp->rd, buffer, length);
}

static ssize_t
dspp_write(struct vmdsp_dsp* dsp, const void* buffer, size_t length) {
   ESDDSPP;

   if (!dspp->wr) {
      if (dspp_start_playback(dspp)) {
	 return length;
      }
   }
   return arts_write(dspp->wr, buffer, length);
}

static int
dspp_set_speed(struct vmdsp_dsp* dsp, int speed) {
   ESDDSPP;

   if (dspp->speed == speed) {
      return 0;
   }
   if (dspp->rd || dspp->wr) {
      vmdsp_warn("Changing sample speed while running (%u => %u)\n", dspp->speed, speed);
   }
   dspp->speed = speed;
   return 0;
}

static int
dspp_set_stereo(struct vmdsp_dsp* dsp, int stereo) {
   ESDDSPP;

   if (dspp->stereo == stereo) {
      return 0;
   }
   if (dspp->rd || dspp->wr) {
      vmdsp_warn("Changing stereo while running (%u => %u)\n", dspp->stereo, stereo);
   }
   dspp->stereo = stereo;
   return 0;
}

static int
dspp_set_format(struct vmdsp_dsp* dsp, int format) {
   ESDDSPP;

   switch (format) {
      case AFMT_U8:
      case AFMT_S16_LE:
         break;
      default:
         format = AFMT_U8;
	 break;
   }
      
   if (dspp->format == format) {
      return 0;
   }
   if (dspp->rd || dspp->wr) {
      vmdsp_warn("Changing format while running (%08X => %08X)\n", dspp->format, format);
   }
   dspp->format = format;
   return 0;
}

static int
dspp_get_format(struct vmdsp_dsp* dsp, int* format) {
   ESDDSPP;

   *format = dspp->format;
   return 0;
}

static const struct vmdsp_operations esd_ops = {
   .cleanup	= artsp_cleanup,
   .new_dsp	= artsp_new_dsp,
   .new_mixer	= NULL,
   .close_dsp	= artsp_close_dsp,
   .close_mixer	= NULL,
   .read	= dspp_read,
   .write	= dspp_write,
   .set_speed	= dspp_set_speed,
   .set_stereo	= dspp_set_stereo,
   .set_format	= dspp_set_format,
   .get_format  = dspp_get_format,
   .set_volume	= NULL,
   .get_volume	= NULL,
   .set_recsrc	= NULL,
   .get_recsrc	= NULL,
};

struct vmdsp_plugin*
vmdsp_plugin_init(const char* cfgfile) {
   struct artsp_plugin* esdp;
   int err;
   
   err = arts_init();
   if (err) {
      vmdsp_warn("aRts initialization failed: %d\n", err);
      return NULL;
   }
   esdp = malloc(sizeof(*esdp));
   if (!esdp) {
      return NULL;
   }
   memset(esdp, 0, sizeof(*esdp));
   esdp->g.d_op = &esd_ops;
   esdp->app = cfgfile;
   return &esdp->g;
}
