/* Coyright (c) 2004-2005  Petr Vandrovec.   Licensed under LGPL-2.1 */

#include "vmdsp_plugin.h"

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/socket.h>

#include <linux/soundcard.h>

#include <esd.h>

struct esdp_plugin {
   struct vmdsp_plugin g;
   const char* server;
   int recsrc;
   char name[ESD_NAME_MAX];
   struct {
      int left, right;
   } volume, igain, mic;
   int mixer;
};

struct esdp_dsp {
   struct vmdsp_dsp g;
   int speed;
   int stereo;
   int format;
   int mode;
   int initialized;
#define DSPINIT_NONE	0
#define DSPINIT_READ	1
#define DSPINIT_WRITE	2
};

struct esdp_mixer {
   struct vmdsp_mixer g;
};

#define container_of(ptr, type, member) ({                      \
        const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)( (char *)__mptr - offsetof(type,member) );})

#define INFO2ESD(x) container_of(x, struct esdp_plugin, g)
#define ESDP(x) struct esdp_plugin* esdp = INFO2ESD(x)

#define DSP2ESD(x) container_of(x, struct esdp_dsp, g)
#define ESDDSPP struct esdp_dsp* dspp = DSP2ESD(dsp)

#define MIXER2ESD(x) container_of(x, struct esdp_mixer, g)
#define ESDMIXERP struct esdp_mixer* mixerp = MIXER2ESD(mixer)

static int
esdp_cleanup(struct vmdsp_plugin* info) {
   ESDP(info);

   free(esdp);
   return 0;
}

static struct vmdsp_dsp*
esdp_new_dsp(struct vmdsp_plugin* info, const char* name, int mode) {
   ESDP(info);
   struct esdp_dsp* dspp;
   
   dspp = malloc(sizeof(*dspp));
   if (dspp) {
      int fd;

      memset(dspp, 0, sizeof(*dspp));
      dspp->g.info = &esdp->g;

      fd = esd_open_sound(esdp->server);
      if (fd < 0) {
         free(dspp);
         errno = ENODEV;
	 return NULL;
      }
      dspp->g.fd = fd;

      dspp->speed = 22050;
      dspp->stereo = 0;
      dspp->format = AFMT_U8;

      dspp->mode = mode;
      return &dspp->g;
   }
   return NULL;
   (void)name;
}

static struct vmdsp_mixer*
esdp_new_mixer(struct vmdsp_plugin* info, const char* name, int mode) {
   ESDP(info);
   struct esdp_mixer* mixerp;
   
   mixerp = malloc(sizeof(*mixerp));
   if (mixerp) {
      int fd;

      memset(mixerp, 0, sizeof(*mixerp));
      mixerp->g.info = &esdp->g;

      fd = vmdsp_open64("/dev/null", mode, 0);
      if (fd < 0) {
         free(mixerp);
	 errno = ENODEV;
	 return NULL;
      }
      mixerp->g.fd = fd;
      return &mixerp->g;
   }
   return NULL;
   (void)name;
}

static int
esdp_close_dsp(struct vmdsp_dsp* dsp) {
   ESDDSPP;
   
   vmdsp_close(dspp->g.fd);
   free(dspp);
   return 0;
}

static int
esdp_close_mixer(struct vmdsp_mixer* mixer) {
   ESDMIXERP;

   vmdsp_close(mixerp->g.fd);
   free(mixerp);
   return 0;
}

static int
esdp_find_player(struct esdp_plugin* esdp, int mode, int* left, int* right) {
   int sid = -1;

   if (esdp->mixer != -1) {
      esd_info_t* ei;

      ei = esd_get_all_info(esdp->mixer);
      if (ei) {
         esd_player_info_t* player;

         for (player = ei->player_list; player; player = player->next) {
	    if ((player->format & ESD_MASK_FUNC) == mode &&
	        !strcmp(player->name, esdp->name)) {
	       sid = player->source_id;
	       *left = player->left_vol_scale;
	       *right = player->right_vol_scale;
	       break;
	    }
	 }
	 esd_free_all_info(ei);
      }
   }
   return sid;
}

static int
esdp_set_volume(struct esdp_plugin* esdp, int mode, int left, int right) {
   int sid;
   int cl, cr;

   sid = esdp_find_player(esdp, mode, &cl, &cr);
   if (sid != -1) {
      left = left / 127;
      right = right / 127;
      if (cl != left || cr != right) {
         esd_set_stream_pan(esdp->mixer, sid, left, right);
      }
      return 0;
   }
   return -1;
}

static int
dspp_start_playback(struct esdp_dsp* dspp) {
   ESDP(dspp->g.info);
   struct {
      u_int32_t proto;
      u_int32_t format;
      u_int32_t rate;
      u_int8_t  name_buf[ESD_NAME_MAX];
   } __attribute__((packed)) spb;

   if (dspp->mode != O_RDWR && dspp->mode != O_WRONLY) {
      errno = EINVAL;
      return -1;
   }
   spb.proto = ESD_PROTO_STREAM_PLAY;
   spb.format  = ESD_STREAM | ESD_PLAY;
   spb.format |= (dspp->format == AFMT_S16_LE) ? ESD_BITS16 : ESD_BITS8;
   spb.format |= dspp->stereo ? ESD_STEREO : ESD_MONO;
   spb.rate = dspp->speed;
   memcpy(spb.name_buf, esdp->name, sizeof(spb.name_buf));
   if (send(dspp->g.fd, &spb, sizeof(spb), MSG_NOSIGNAL) != sizeof(spb)) {
      errno = EINVAL;
      return -1;
   }
   /* Well, just pray... set_volume must be done after ESD performs
      request above, but it does not ackowledge operation above in
      any way... So if volume change does not work for you, try adding
      some sleep() here */
   esdp_set_volume(esdp, ESD_PLAY, esdp->volume.left, esdp->volume.right);
   dspp->initialized = DSPINIT_WRITE;
   return 0;
}

static int
dspp_start_record(struct esdp_dsp* dspp) {
#if 0
   /* No way... ESD_MONITOR needs some other player running, otherwise 
      no data are provided, and for ESD_RECORD we would need
      non-blocking ops. */
   ESDP(dspp->g.info);
   struct {
      u_int32_t proto;
      u_int32_t format;
      u_int32_t rate;
      u_int8_t  name_buf[ESD_NAME_MAX];
   } __attribute__((packed)) src;
   
   if (dspp->mode != O_RDWR && dspp->mode != O_RDONLY) {
      errno = EINVAL;
      return -1;
   }
   if (esdp->recsrc == SOUND_MIXER_MIC) {
      src.proto = ESD_PROTO_STREAM_REC;
      src.format = ESD_STREAM | ESD_RECORD;
   } else {
      src.proto = ESD_PROTO_STREAM_MON;
      src.format = ESD_STREAM | ESD_MONITOR;
   }
   src.format |= (dspp->format == AFMT_S16_LE) ? ESD_BITS16 : ESD_BITS8;
   src.format |= dspp->stereo ? ESD_STEREO : ESD_MONO;
   src.rate = dspp->speed;
   memcpy(src.name_buf, esdp->name, sizeof(src.name_buf));
   if (send(dspp->g.fd, &src, sizeof(src), MSG_NOSIGNAL) != sizeof(src)) {
      errno = EINVAL;
      return -1;
   }
   if (src.proto == ESD_PROTO_STREAM_REC) {
      esdp_set_volume(esdp, ESD_RECORD, esdp->mic.left, esdp->mic.right);
   } else {
      esdp_set_volume(esdp, ESD_MONITOR, esdp->igain.left, esdp->igain.right);
   }
   dspp->initialized = DSPINIT_READ;
   return 0;
#else
   errno = EINVAL;
   return -1;
   (void)dspp;
#endif
}

static ssize_t
dspp_read(struct vmdsp_dsp* dsp, void* buffer, size_t length) {
   ESDDSPP;

   switch (dspp->initialized) {
      case DSPINIT_NONE:
         if (dspp_start_record(dspp)) {
	    return -1;
	 }
      case DSPINIT_READ:
         return recv(dspp->g.fd, buffer, length, 0);
      default:
         errno = EINVAL;
	 return -1;
   }
   return 0;
}

static ssize_t
dspp_write(struct vmdsp_dsp* dsp, const void* buffer, size_t length) {
   ESDDSPP;

   switch (dspp->initialized) {
      case DSPINIT_NONE:
         if (dspp_start_playback(dspp)) {
	    return -1;
	 }
      case DSPINIT_WRITE:
         return send(dspp->g.fd, buffer, length, MSG_NOSIGNAL);
      default:
         errno = EINVAL;
	 return -1;
   }
}

static int
dspp_set_speed(struct vmdsp_dsp* dsp, int speed) {
   ESDDSPP;

   if (dspp->speed == speed) {
      return 0;
   }
   if (dspp->initialized) {
      vmdsp_warn("Changing sample speed while running (%u => %u)\n", dspp->speed, speed);
   }
   dspp->speed = speed;
   return 0;
}

static int
dspp_set_stereo(struct vmdsp_dsp* dsp, int stereo) {
   ESDDSPP;

   if (dspp->stereo == stereo) {
      return 0;
   }
   if (dspp->initialized) {
      vmdsp_warn("Changing stereo while running (%u => %u)\n", dspp->stereo, stereo);
   }
   dspp->stereo = stereo;
   return 0;
}

static int
dspp_set_format(struct vmdsp_dsp* dsp, int format) {
   ESDDSPP;

   switch (format) {
      case AFMT_U8:
      case AFMT_S16_LE:
         break;
      default:
         format = AFMT_U8;
	 break;
   }
      
   if (dspp->format == format) {
      return 0;
   }
   if (dspp->initialized) {
      vmdsp_warn("Changing format while running (%08X => %08X)\n", dspp->format, format);
   }
   dspp->format = format;
   return 0;
}

static int
dspp_get_format(struct vmdsp_dsp* dsp, int* format) {
   ESDDSPP;

   *format = dspp->format;
   return 0;
}

static int
mixerp_set_volume(struct vmdsp_mixer* mixer, int channel, int* left, int* right) {
   ESDMIXERP;
   ESDP(mixerp->g.info);

   switch (channel) {
      case SOUND_MIXER_MIC:
         esdp->mic.left = *left;
	 esdp->mic.right = *right;
	 esdp_set_volume(esdp, ESD_RECORD, esdp->mic.left, esdp->mic.right);
	 break;
      case SOUND_MIXER_IGAIN:
         esdp->igain.left = *left;
	 esdp->igain.right = *right;
	 esdp_set_volume(esdp, ESD_MONITOR, esdp->igain.left, esdp->igain.right);
	 break;
      case SOUND_MIXER_VOLUME:
         esdp->volume.left = *left;
	 esdp->volume.right = *right;
	 esdp_set_volume(esdp, ESD_PLAY, esdp->volume.left, esdp->volume.right);
	 break;
      default:
         *left = 0;
	 *right = 0;
	 break;
   }
   return 0;
}

static int
mixerp_get_volume(struct vmdsp_mixer* mixer, int channel, int* left, int* right) {
   ESDMIXERP;
   ESDP(mixerp->g.info);

   switch (channel) {
      case SOUND_MIXER_MIC:
         *left = esdp->mic.left;
	 *right = esdp->mic.right;
	 break;
      case SOUND_MIXER_IGAIN:
         *left = esdp->igain.left;
	 *right = esdp->igain.right;
      case SOUND_MIXER_VOLUME:
         *left = esdp->volume.left;
	 *right = esdp->volume.right;
	 break;
      default:
         *left = 0;
         *right = 0;
	 break;
   }
   return 0;
}

static int
mixerp_set_recsrc(struct vmdsp_mixer* mixer, int* channel) {
   ESDMIXERP;
   ESDP(mixerp->g.info);
   int ch = *channel;

   switch (ch) {
      case SOUND_MASK_MIC:
         esdp->recsrc = SOUND_MIXER_MIC;
      case SOUND_MASK_IGAIN:
         esdp->recsrc = SOUND_MIXER_IGAIN;
	 break;
      default:
         vmdsp_warn("Unsupported recording channel %u, using IGAIN instead\n", ch);
	 esdp->recsrc = SOUND_MIXER_IGAIN;
	 *channel = SOUND_MASK_IGAIN;
	 break;
   }
   return 0;
}

static int
mixerp_get_recsrc(struct vmdsp_mixer* mixer, int* channel) {
   ESDMIXERP;
   ESDP(mixerp->g.info);

   *channel = 1 << esdp->recsrc;
   return 0;
}

static const struct vmdsp_operations esd_ops = {
   .cleanup	= esdp_cleanup,
   .new_dsp	= esdp_new_dsp,
   .new_mixer	= esdp_new_mixer,
   .close_dsp	= esdp_close_dsp,
   .close_mixer	= esdp_close_mixer,
   .read	= dspp_read,
   .write	= dspp_write,
   .set_speed	= dspp_set_speed,
   .set_stereo	= dspp_set_stereo,
   .set_format	= dspp_set_format,
   .get_format  = dspp_get_format,
   .set_volume	= mixerp_set_volume,
   .get_volume	= mixerp_get_volume,
   .set_recsrc	= mixerp_set_recsrc,
   .get_recsrc	= mixerp_get_recsrc,
};

struct vmdsp_plugin*
vmdsp_plugin_init(const char* cfgName) {
   struct esdp_plugin* esdp;
   
   esdp = malloc(sizeof(*esdp));
   if (esdp) {
      extern int esd_no_spawn;
      extern void esd_config_read(void);
      const char* server;

      memset(esdp, 0, sizeof(*esdp));
      esdp->g.d_op = &esd_ops;
      esdp->recsrc = SOUND_MIXER_MIC;
      
      esd_config_read();
      esd_no_spawn = 1;
   
      server = getenv("ESPEAKER");
      if (!server) {
         server = "";
      }
      esdp->server = server;
      snprintf(esdp->name, ESD_NAME_MAX, "vmware-%u-%lu-%08X-%s", getpid(), time(NULL), rand(), cfgName);
      esdp->volume.left = esdp->volume.right = 0x7F00;
      esdp->igain.left  = esdp->igain.right  = 0x7F00;
      esdp->mic.left    = esdp->mic.right    = 0x7F00;

      esdp->mixer = esd_open_sound(esdp->server);
   }
   return &esdp->g;
}
